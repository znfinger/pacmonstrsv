# README #

### PACmonSTR-SV Overview ###

This is a PACmonSTR-SV toolkit, including both the dotplot projection method described briefly in:

Characterizing and Overriding the Structural Mechanism of the Quizartinib-Resistant FLT3 "Gatekeeper" F691L Mutation with PLX3397

(https://www.ncbi.nlm.nih.gov/pubmed/25847190)

and then extended and used without description in:

Assembly and diploid architecture of an individual human genome via single-molecule technologies.

(https://www.ncbi.nlm.nih.gov/pubmed/26121404)

In addition to an embodiment of PACmonSTR-SV that can be used in linear time on error corrected reads or assembled contigs, several tools are also included for generating random SVs for testing purposes. A much simpler inversion caller is included that will be used in a coming paper collaboration.

Additional tools in this form will be written as needed or wanted.

### How do I get set up? ###

## Dependencies
### Packages
```
Python/2.7.6
numpy/1.13.1
scipy/0.19.1
matplotlib/2.0.2
hmmlearn/0.2.0
pbcore/1.2.10
Cython/0.26
pysam/0.11.2.2
h5py/2.7.0
sklearn/0.19.0
```
### Tools
```
none
```
Because the tool requires paired aligned reads, an aligner is required.
 

## Install
```
git clone https://bitbucket.org/znfinger/pacmonstrsv.git
cd pacmonstrsv
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python setup.py install
```

## Note
Input data should be arranged in paired fasta files, as below. The names are not used, so may be arbitrary.

# file 1
```
>read1
ACGATTGCA
>read2
ACGAGGGTTGCA
>read3
ACGATTGATATATATCA
```

# file2
```
>ref1
ACGATTGCA
>ref2
ACGATTGCA
>ref3
ACGATTGATATCA
```
Here, read1 has been mapped to some reference interval ref1, read2 to ref2, and so on.

## Running SVsim sample SV generator - generating 1000 sequence pairs with SVs.
```
python ./scripts/makeSV_dataset.py 1000
```

## Running inversion tool (True indicates that dotplots should be generated.)
```
python ./scripts/pacmonstrsv_inversion_simple.py Q.fasta T.fasta True
```

