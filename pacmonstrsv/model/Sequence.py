from pbcore.io.FastaIO import FastaReader
rcDict = dict(zip('ACGTWSMKRYBDHVNacgtwsmkrybdhvn-','TGCAWSKMYRVHDBNtgcawskmyrvhdbn-'))


def revcomp(seq):
   """Return the reverse complement of a string:

   :param seq: DNA Sequence to be reverse complemented
   :type seq: str
   :returns: A new string that is the reverse complement of seq
   :rtype: string

   """
   return ''.join([rcDict[nuc] for nuc in  seq[::-1]])

def readFastaToDict (fn):
   '''
   Returns a dictionary of fasta sequences where the key
   is the fasta entry name
   '''
   seqDict = {}
   for entry in FastaReader(fn):
      seqDict[entry.name.split()[0]] = entry.sequence
   return seqDict
   

def codons(seq, frame): #breaks a sequence into codons according to the specified frame
   if frame in [1,2,3]:
       return [seq[frame-1:][3*i:3*i+3] for i in range(len(seq)//3)]
   elif frame in [-1,-2,-3]:
       rcseq = revcomp(seq)
       return [rcseq[-frame-1:][3*i:3*i+3] for i in range(len(rcseq)//3)]
   elif frame == 0:#6 frame translation option
       frames = []
       rcseq = revcomp(seq)
       print rcseq
       for fr in [1,2,3]:
           frames.append([seq[fr-1:][3*i:3*i+3] for i in range(len(seq)//3)])
       for fr in [-1,-2,-3]:
           frames.append([rcseq[-fr-1:][3*i:3*i+3] for i in range(len(rcseq)//3)])
       return frames
   else:
       print 'Improper frame specified.'

gencode = {
   'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
   'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
   'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
   'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
   'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
   'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
   'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
   'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
   'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
   'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
   'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
   'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
   'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
   'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
   'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
   'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W'}

def translate(seq , frame):
   if frame in [1,2,3]:
       return ''.join([gencode.get(seq[frame-1:][3*i:3*i+3],'X') for i in range(len(seq)//3)])
   elif frame in [-1,-2,-3]:
       rcseq = revcomp(seq)
       return ''.join([gencode.get(rcseq[-frame-1:][3*i:3*i+3],'X') for i in range(len(rcseq)//3)])
   elif frame == 0:#6 frame translation option
       frames = []
       rcseq = revcomp(seq)
       print rcseq
       for fr in [1,2,3]:
           frames.append(''.join([gencode.get(seq[fr-1:][3*i:3*i+3],'X') for i in range(len(seq)//3)]))
       for fr in [-1,-2,-3]:
           frames.append( ''.join([gencode.get(rcseq[-fr-1:][3*i:3*i+3],'X') for i in range(len(rcseq)//3)]))
       return frames
   else:
       print 'Improper frame specified.'


######
#a tool for generating pairs of sequence that differ by one or a number of structural variations
class Seq:
    def __init__(self,seqLength):
        self.flag = 0
        self.history = []
        try:
            self.baselength = int(seqLength)
            self.baseSeq = "".join([random.choice('ACGT') for x in range(int(self.baselength))])
            self.seq = self.baseSeq
            self.length = len(self.seq)
        #if seq is a string, then pass it directly to the baseSeq
        except ValueError:
            self.baseSeq = seq
            self.seq= seq
            self.baselength = len(self.baseSeq)
            self.length = len(self.seq)
    def printHistory(self):
        return '-'.join(map(lambda x: '.'.join(map(str,x)),self.history))
    def Duplication(self,start,stop,newLoc,inverted=False):
        if inverted:
            self.seq = self.seq[:newLoc]+rc(self.seq[start:stop])+self.seq[newLoc:]
        else:
            self.seq = self.seq[:newLoc]+self.seq[start:stop]+self.seq[newLoc:]
        self.length = len(self.seq)
        self.flag = 1
        self.history.append(('Dup',start,stop,newLoc,inverted))
    def Deletion(self, start, stop):
        self.seq = self.seq[:start]+self.seq[stop:]
        self.length = len(self.seq)
        self.flag = 1
        self.history.append(('Del',start,stop))
    def Inversion(self,start, stop):
        self.seq = self.seq[:start]+rc(self.seq[start:stop])+self.seq[stop:]
        self.length = len(self.seq)
        self.flag = 1
        self.history.append(('Inv',start,stop))
    def Insertion(self,loc, length):
        newSeq = ''.join([random.choice('ACGT') for _ in range(length)])
        self.seq = self.seq[:loc]+newSeq+self.seq[loc:]
        self.flag = 1
        self.history.append(('Ins',loc,newSeq))
    def SubstitionSV(self,start, stop, length):
        newSeq = ''.join([random.choice('ACGT') for _ in range(length)])
        self.seq = self.seq[:start]+newSeq+self.seq[stop:]
        self.flag = 1
        self.history.append(('Sub',start,stop,newSeq))
    def DupDel(self,start,stop, newLoc, inverted=False):
        """
        adds a duplication into the base seq.
        Do not use after applying other operations.
        self.flag check
        """
        if self.flag == 0:
            if inverted:
                self.baseSeq = self.baseSeq[:newLoc]+rc(self.baseSeq[start:stop])+self.baseSeq[newLoc:]
                #self.history.append(('DupDel',start,stop,newLoc,inverted))
            else:
                self.baseSeq = self.baseSeq[:newLoc]+self.baseSeq[start:stop]+self.baseSeq[newLoc:]
            self.baselength = len(self.baseSeq)
        else:
            pass
