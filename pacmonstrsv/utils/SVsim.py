#!/usr/bin/env python 

#A sequence class for generating toy sequences of random composition
#   and structural variations in those sequences.

#Takes as an argument EITHER a sequence length or an actual sequence
#   (in cases where using a real sequence is necessary). A self.history
#   variable is included which keeps a record of all SVs applied to the
#   base sequence to generate the current sequence.

import random

import numpy as np

from pacmonstrsv.model.Sequence import revcomp as rc

class Seq:
    def __init__(self,seqLength):
        self.flag = 0
        self.history = []
        # extra data about bases, arrays of scores, etc. keyed
        #    on score type name, value is numpy array
        self.tracks = {} 
        try:
            #If seqLength is a string rather than an int, this raises an error
            self.baselength = int(seqLength)
            self.baseSeq = "".join([random.choice('ACGT') for x in range(int(self.baselength))])
            #now with randomly placed, possibly recursively
            #    generated short tandem repeats
            while random.random < .1:
                if random.random() < .05:
                    str_pos = random.randint(0,self.baselength)
                    str_len = random.randint(2,6)
                    if random.random() < .9:
                        str_cn = random.randint(5,50)
                    else:
                        str_cn = random.randint(50,500)
                    self.baseSeq = self.baseSeq[:str_pos] \
                                   + self.baseSeq[str_pos:str_pos+str_len]*(str_cn-1) \
                                   + self.baseSeq[str_pos:]
                    self.history.append(("STR",str_pos,str_len,str_cn))
            self.seq = self.baseSeq
            self.length = len(self.seq)

        #if seq is a string, then pass it directly to the baseSeq
        #  used for generating SVs on sequences taken from an actual genome
        #  rather than those generated from a uniform and random base
        #  frequency distribution.
        except ValueError:
            self.baseSeq = seqLength
            self.seq= seqLength
            self.baselength = len(self.baseSeq)
            self.length = len(self.seq)
            
    def makeOM(self,recog):
        self._OM = []
        k = len(recog)
        motifs = set([recog, rc(recog)])
        for pos in range(self.length-k):
            if self.seq[pos:pos+k] in motifs:
                self._OM.append(pos)
        self._OM.append(self.length)              
        self._OM = np.array(self._OM)
            
    #the tracks - numpy arrays indexed to the self.seq sequence
    def modifyTrack(name,nparray):
        self.tracks[name] = nparray
    def getTrack(name):
        try:
            return self.tracks[name]
        except KeyError:
            return -1
    def help(self):
        print """
        duplication:

            Duplication of a subsequence elsewhere in the sequence. 

            start         The start of the duplicated of duplicated substring.
            stop          The end of duplicated substring.
            new location  The new location for the interval indicated by start->stop values.

        deletion:

            Deletion of an interval.

            start         The start of the deleted interval.
            stop          The end of the deleted interval.

        inversion:

            Inversion of an interval.

            start         The start of the inverted interval.
            stop          The end of the inverted interval.

        insertion:

            Insertion of a sequence at a position.

            loc           The location of the insertion.
            length        The length of the randomly generated inserted sequence.

        substitutionSV:

            A compound event whereby an interval is replaced with some other sequence.

            start         The start of the deleted interval.
            stop          The end of the deleted interval.
            length        The length of the randomly generated sequence that 
                          replaces the deleted interval between start and stop position.

        dupDel:
            
            This adds a duplication in the self.baseSeq rather than the self.seq sequence,
            enabling the exploration of SVs when the base sequences contains repeat elements.
            This function is protected such that it can only be applied to the baseSeq,
            and only BEFORE any operations have been applied to the self.seq.
            Override can be done by setting self.flag = 0 before applying self.dupDel().

            start            Start of the interval to be duplicated.
            stop             End of the interval to be duplicated.
            newLoc           The new location for the duplicated sequence in the reference.
            inverted=False   Boolean for whether the new copy is inverted or not. Set to 
                             false by default.

        indelNoise:
         
            Introduces indel noise into the read, with insertions and deletions in equal 
            but stochastic measure, and in a context free manner.

            freq          Frequency of indels. Set between [0,1).

        substNoise:

            Introduces substition noise in the read. Currently no GC bias.

            freq         Frequency of substitutions. Set between [0,1).


        """
    def printHistory(self):
        return '-'.join(map(lambda x: '.'.join(map(str,x)),self.history))
    def random(self, minSize = 50):
        """
        Generates one random SV event of any type, location and size.
        """
        eventType = random.choice(["dup","del","inv","ins","subSV","dupDel"])
        strand = random.choice([False,True])
        if eventType == "dup":
            start = random.randint(0,len(self.seq)-minSize)
            stop = random.randint(start,len(self.seq))
            newLoc = random.randint(0,len(self.seq))
            self.duplication(start,stop,newLoc, inverted=strand)
        elif eventType == "del":
            start = random.randint(0,len(self.seq)-minSize)
            stop = random.randint(start,len(self.seq))
            self.deletion(start,stop)
        elif eventType == "inv":
            start = random.randint(0,len(self.seq)-minSize)
            stop = random.randint(start,len(self.seq))
            self.inversion(start,stop)
        elif eventType == "ins":
            loc = random.randint(0,len(self.seq))
            size = random.randint(minSize, 500)
            self.insertion(loc,size)
        elif eventType == "subSV":
            start = random.randint(0,len(self.seq)-minSize)
            stop = random.randint(start,len(self.seq))
            newSize = random.randint(minSize, 10000)
            self.substitutionSV(start,stop,newSize)
        elif eventType == "dupDel" and self.flag == 0:
            start = random.randint(0,len(self.baseSeq)-minSize)
            stop = random.randint(start,len(self.baseSeq))
            newLoc = random.randint(0,len(self.baseSeq))
            self.dupDel(start,stop,newLoc, inverted=strand)
        else:
            return 0
        return 1

    def duplication(self,start,stop,newLoc,inverted=False):
        if inverted:
            from pacmonstrsv.model.Sequence import revcomp as rc
            self.seq = self.seq[:newLoc]+rc(self.seq[start:stop])+self.seq[newLoc:]
        else:
            self.seq = self.seq[:newLoc]+self.seq[start:stop]+self.seq[newLoc:]
        self.length = len(self.seq)
        self.flag = 1
        self.history.append(('Dup',start,stop,newLoc,inverted))
        self.tracks = {}
    def deletion(self, start, stop):
        self.seq = self.seq[:start]+self.seq[stop:]
        self.length = len(self.seq)
        self.flag = 1
        self.history.append(('Del',start,stop))
        self.tracks = {}
    def inversion(self,start, stop):
        from pacmonstrsv.model.Sequence import revcomp as rc
        self.seq = self.seq[:start]+rc(self.seq[start:stop])+self.seq[stop:]
        self.length = len(self.seq)
        self.flag = 1
        self.history.append(('Inv',start,stop))
        self.tracks = {}
    def insertion(self,loc, length):
        import random
        newSeq = ''.join([random.choice('ACGT') for _ in range(length)])
        self.seq = self.seq[:loc]+newSeq+self.seq[loc:]
        self.flag = 1
        self.history.append(('Ins',loc,newSeq))
        self.tracks = {}
    def substitutionSV(self,start, stop, length):
        import random
        newSeq = ''.join([random.choice('ACGT') for _ in range(length)])
        self.seq = self.seq[:start]+newSeq+self.seq[stop:]
        self.flag = 1
        self.history.append(('Sub',start,stop,newSeq))
        self.tracks = {}

    def dupDel(self,start,stop, newLoc, inverted=False):
        if self.flag == 0:
            if inverted:
                from pacmonstrsv.model.Sequence import revcomp as rc
                self.baseSeq = self.baseSeq[:newLoc] \
                               + rc(self.baseSeq[start:stop]) \
                               + self.baseSeq[newLoc:]
                self.history.append(('DupDel',start,stop,newLoc,inverted))
            else:
                self.baseSeq = self.baseSeq[:newLoc] \
                               + self.baseSeq[start:stop] \
                               + self.baseSeq[newLoc:]
            self.baselength = len(self.baseSeq)
            #randomly pick which copy to delete
            if random.random() < 0.5:
                self.seq = self.baseSeq[:start]+self.baseSeq[stop:]
            else:
                self.seq = self.baseSeq[:newLoc] \
                           + self.baseSeq[newLoc + (stop-start):]
            self.tracks = {}
        else:
            pass

    def indelNoise(self,freq):
        #assumes random indels, agnostic of local content
        #assumes P(in) == P(del)
        import random
        out = []
        pos = 0
        while pos < len(self.seq):
            if random.random() < freq:
                if random.random() < .5:
                    out.append(random.choice('ACGT'))
                else:
                    pos += 1
            else:
                out.append(self.seq[pos])
                pos += 1
        self.seq = ''.join(out)
        self.tracks = {}
    def substNoise(self,frequency):
        import random
        out = []
        for pos in xrange(len(self.seq)):
            if random.random() < frequency:
                out += random.choice('ACGT')
            else:
                out += self.seq[pos]
        self.seq = ''.join(out)
        self.tracks = {}      
      
####

class OpticalMap:
    """
    A pared down optical map class based on bashirlab.io.BNXio. 
    For braided string graph assembly purposes.
    """
    def __init__(self,moleculeId,seq,motifs,QX01 = None,QX02=None):
        self.__name__ = "OpticalMap"
        try:
            if type(motifs) == str:
                self._sequence = []
                k = len(motifs)
                for pos in range(len(seq)-k):
                    if seq[pos:pos+k] == motifs:
                        self._sequence.append(pos)
                self._sequence.append(len(seq))              
                self._sequence = np.array(self._sequence)
            elif type(motifs) == set:
                print "using a set to mash"
                print " ".join(motifs)
                self._sequence = []
                k = set([len(x) for x in motifs])
                if len(k) != 1:
                    raise AssertionError("motifs are different \
                    lengths. Not ready to handle this case.")
                k = list(k)[0]
                for pos in range(len(seq)-k):
                    print seq[pos:pos+k] 
                    if seq[pos:pos+k] in motifs:
                        self._sequence.append(pos)
                self._sequence.append(len(seq))
                self._sequence = np.array(self._sequence)
            else:
                raise AssertionError("Collections of motifs \
                must be passed as a set object.")
            self._moleculeId = int(moleculeId)
            self._Length = self._sequence[-1]
            self._moleculeId = int(moleculeId)
            self._QX01 = QX01
            self._QX02 = QX02
        except AssertionError:
            raise ValueError("Invalid optical map record data.")

    def makeIND(self):
        self._IND = [self._sequence[x] - self._sequence[x-1] for x in range(1,len(self._sequence))]

    @property
    def moleculeId(self):
        """
        The id of the map sequence in the bnx file, taken from the header information.
        """
        return self._moleculeId

    @property
    def sequence(self):
        """
        The sequence for the record as present in the FASTA file.
        (Newlines are removed but otherwise no sequence normalization
        is performed).
        """
        return self._sequence

    @property
    def QX01(self):
        """
        The sequence for the record as present in the FASTA file.
        (Newlines are removed but otherwise no sequence normalization
        is performed).
        """
        return self._QX01

    @property
    def QX02(self):
        """
        The sequence for the record as present in the FASTA file.
        (Newlines are removed but otherwise no sequence normalization
        is performed).
        """
        return self._QX02

    @property
    def length(self):
        """
        Get the length of the DNA map.
        """
        return self._Length

    def reverse(self):
        """
        Return a new OMRecord with the reverse map sequence.
        Optionally, supply a name.

        This may not be necessary. D-band can be done in fwd or
        reverse orientation by swapping the i-j and j-i.
        May be necessary for OMblast alignment though.
        """
        return OpticalMapRecord(self.moleculeId, self._sequence[::-1] \
                                ,length, QX01=self._QX01[::-1], \
                                QX02=self._QX002[::-1])

    def __len__(self):
        return self.length

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return (self.sequence == other.sequence)
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        """
        Output a string representation of this record, observing
        standard conventions about sequence wrapping.
        """
        preface = "1\t{}\n".format('\t'.join(map(str,self.sequence)))
        if self.QX01 is None:
            output = preface
        elif self.QX02 is None:
            output = "{}QX11\t{}".format(preface , \
                                           '\t'.join(map(str, self.QX01)))
        else:
            output = "{}QX11\t{}\nQX12\t{}" % (preface, \
                                               '\t'.join(map(str, self.QX01)), \
                                               '\t'.join(map(str, self.QX02)))
        return output

