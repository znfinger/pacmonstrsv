#!/usr/bin/env python -o

#Inputs:
# A pair of fasta files where file1 is assembled sequences
# and file2 is the mapping interval in a second genome (typically a 
# reference genome).

#Outputs:
# 

import sys
import os

import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle as rect

from pacmonstrsv.utils.PACmonSTRSV import inversion
from pacmonstrsv.utils.PACmonSTRSV import DotPlot

from pacmonstrsv.utils.SVsim import Seq

#reasonably rational handling of various common
#    input types:
#    0. An oscar phased assembly file (>1 >H1 >H2)
#    1. fasta pairs (a Q.fasta and T.fasta)
#    2. .sam file with reference fasta and index
#    3. .bam file with reference fasta and index
#    4. a single .m5 alignment file from blasr

Qreads = []
Treads = []

Ext = sys.argv[1].split(".")[-1]


print Ext == "fofn"

if Ext == "fofn":
    print "using interpreter for Oscar's fofn format."
    #oscars 1, H1, H2 format
    # only plots H1:ref and H2:ref
    # does not try to plot H1:H2 yet
    from pacmonstrsv.io.FastaIO import FastaReader as fr
    locs = []
    H1flag = False
    H2flag = False
    for fn in open(sys.argv[1]):
        print fn
        try:
            for entry in fr(fn.strip()):
                if entry.name not in ["H1","H2"]: #reference sequence
                    ref = entry
                if entry.name == "H1": #haplotype 1 sequence
                    Qreads.append(entry)
                    Treads.append(ref)
                    locs.append((ref.name,entry.name))
                    H1flag = entry.name
                if entry.name == "H2": #haplotype 2 sequence
                    Qreads.append(entry)
                    Treads.append(ref)
                    locs.append((ref.name,entry.name))
                    if H1flag != False:
                        Treads.append(Treads[-1])
                        Qreads.append(entry)
                        locs.append((H1flag,entry.name,ref.name))
        except:
            print "file is not valid, empty,broker or otherwise buggered."
elif Ext in ("fa","fasta"):
    from pacmonstrsv.io.FastaIO import FastaReader as fr
    for entry in fr(sys.argv[1]):
        Qreads.append(entry)
    for entry in fr(sys.argv[2]):
        Treads.append(entry)
elif Ext in ("sam","bam","cram") :
    import pysam

    mappings = pysam.AlignmentFile(sys.argv[1],{"bam":"rb","sam":"r","cram":"cr"}[Ext])

    try:
        refGenome = pysam.FastaFile(sys.argv[2])
    except:
        print "Probably missing the fasta index file."
        exit()

    for entry in mappings:
        if entry.is_unmapped:
            continue
        try:
            point = 0
            Qreads.append(entry.query_alignment_sequence)
            point += 1
            loc = "{}:{}-{}".format(entry.reference_name,
                                    entry.reference_start+1,
                                    entry.reference_end)
            point += 1
            Treads.append(refGenome.fetch(region=loc))
        except:
            print ">>>",point
elif sys.argv[1].split(".")[-1] in ("m5") :
    for line in open(sys.argv[1]):
        l = line.strip().split()
        Qreads.apend(l[16].upper().replace("-",""))
        Treads.apend(l[18].upper().replace("-",""))

print "Sequences loaded into memory. Starting PACmonSTR-SV."

#plot things if specified. 
try:
    assert sys.argv[3]
    plotFlag = True
except:
    plotFlag = False    

try:
    assert len(Qreads) == len(Treads)
except:
    raise ValueError("Fasta files contain different numbers of sequences.")


print ">>>1"
if Ext == "fofn":
    pos_locsOut = open("pos_{}".format(sys.argv[1]),'w')
    neg_locsOut = open("neg_{}".format(sys.argv[1]),'w')
else:
    posOutQ = open('pos_{}'.format(sys.argv[1]),'w')
    posOutT = open('pos_{}'.format(sys.argv[2]),'w')
    negOutQ = open('neg_{}'.format(sys.argv[1]),'w')
    negOutT = open('neg_{}'.format(sys.argv[2]),'w')
    maybeOutQ = open('maybe_{}'.format(sys.argv[1]),'w')
    maybeOutT = open('maybe_{}'.format(sys.argv[2]),'w')

print ">>>2"
#parameters live here
#  b: used to generate the transition probabilities for the HMM.
#  k: kmer size.
b = .01
k = 40
Tr = np.array([[1.-b,b],[b,1.-b]])
print ">>>3"
for pos in range(len(Qreads)):
    Q = Qreads[pos].sequence
    T = Treads[pos].sequence
    print "Plotting",Qreads[pos].name,"vs",Treads[pos].name
#    inT = inversion(Q,T,kmer=k,transmat = Tr)
    inQ = inversion(T,Q,kmer=k,transmat = Tr)
    if plotFlag:
        plt.clf()
        fig, ax = DotPlot(Q,T)
    invFlag = False
    if len(inQ):
        #create a new Seq object and fix all inversions
        # then re-evaluate the edited sequence for inversions.
        newQ = Seq(Q)
        for y in inQ:
            start = max([ 0, y[0] - k / 2  ]) 
            stop = min([ y[0] + k / 2  , len(Q)]) 
            newQ.inversion(start,stop)
        # maybe draw the corrected sequence here to visually 
        #    confirm at some point?
        newInQ = inversion(newQ.seq,T,kmer=k,transmat= Tr)
        inT = []
        if len(Q) > len(newInQ):
            # If seq could be corrected in at least one location by applying
            #    inversions, then find each inversion by partitioning Q to flank
            #    each event, and identify the inverted interval in T. Draw a
            #    rectangle around it and store to some final output for
            #    coordinating with the mapped locus in the genome.
            for y in inQ:
                start = max([ 0, y[0] - k / 2  ]) 
                stop = min([ y[1] + k / 2  , len(Q)]) 
                subQ = Q[start:stop]
                # events here are all indexed into the full length ofT
                inT.extend(inversion(subQ,T,kmer=k,transmat=Tr))

        # At this point we have all putative inverted intervals in Q
        #   and a set of intervals in T for which there is a locally
        #   matching inverted interval in Q. Now go through and identify
        #   all the pairs that match in Q and T.

        # something smarter here might be reasonable wherein better matching
        #    between Q size and T size will prevent having to double-count events
        #    when there is more than one event AND repeat structures per interval.
        #    This is trivial though, since this script operates as a simple classifier.
        print ">>>4"
        for y in inQ:
            for x in inT:
                invConfirm= inversion(Q[y[0]:y[1]],T[x[0]:x[1]], kmer = k, transmat = Tr)
                if invConfirm:
                    invFlag = True
                    r = rect((x[0],y[0]),x[1]-x[0],y[1]-y[0],alpha=0.4)
                    if plotFlag:
                        ax.add_patch(r)
                else:
                    r = rect((x[0],y[0]),x[1]-x[0],y[1]-y[0],color="0.3",alpha=0.05) 
                    if plotFlag:
                        ax.add_patch(r)
    print ">>>5"
    #less clear case where inversion could be masked by its position in a
    #  duplicated sequence
    if Ext == "fofn":
        print "invFlag",invFlag
        if invFlag:
            pos_locsOut.write("{}\n".format('\t'.join(locs[pos])))
        else:
            print '\t'.join(locs[pos])
            neg_locsOut.write("{}\n".format('\t'.join(locs[pos])))
        if plotFlag:
            plt.savefig('images/{}.png'.format('.'.join(locs[pos])))
            plt.close('all')
    else:
        if invFlag:
            posOutQ.write(">{}\n{}\n".format(Qreads[pos].name, Qreads[pos].sequence))
            posOutT.write(">{}\n{}\n".format(Treads[pos].name, Treads[pos].sequence))
        else:
            negOutQ.write(">{}\n{}\n".format(Qreads[pos].name, Qreads[pos].sequence))
            negOutT.write(">{}\n{}\n".format(Treads[pos].name, Treads[pos].sequence))
        if plotFlag:
            plt.savefig('images/{}.png'.format('.'.join(Qreads[pos].name.split("_")[:-1])))
            plt.close('all')






