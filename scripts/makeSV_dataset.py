#!/usr/bin/env python

#####
#This tool generates a corpus of randomly generated sequences, each with a
#    randomly selected number and type of structural variant events in it.
#    This code has been largely hard-wired so that the following is true of
#    the generated sequences:
#
#    1. There are between 0 and 5 SVs per sequence.
#
#    2. The indel noise is set to 1 inserted or deleted base per kb.
#
#    3. Substitutions occur at 1 per 10 kb.
#
#    Settings were chosen to emulate very high quality reads with a background
#    of SNVs. If the user's desire is to emulate raw read quality, or
#    some other set of conditions, that can be altered below.
#
#The script outputs are as follows:
#
#   1. Q.fasta    A .fasta file with reads containing some number of SVs, along
#                 with the indicated indel and substitution noise.
#   2. T.fasta    A file with the baseSeq for each generated SV read in Q.fasta.
#                 These files are intended to be treated as the ground truth
#                 regarding any noise, errors, or SVs.
#   3. reads.dat  A file describing the SVs contained in each of the reads in
#                 the Q.fasta file. This can be used to verify the results of
#                 a structural variant finder.

from pacmonstrsv.utils.SVsim import Seq 
import random as r
import sys

#how many examples does user want to generate
reads = int(sys.argv[1])

tout = open('T.fasta','w')
qout = open('Q.fasta','w')
eout = open('reads.dat','w')

minSize = 50
for x in range(reads):
    a = Seq(10000)
    c = r.randint(0,5)
    events = 0
    for b in range(c):
        events += a.random()
    #indel variants
    a.indelNoise(.001)
    #emulating SNVs
    a.substNoise(.0001)
    qout.write(">{}_{}_q\n{}\n".format(x,events,a.seq))
    tout.write(">{}_{}_t\n{}\n".format(x,events,a.baseSeq))
    eout.write(">{}:{}:{}\n".format(x,events,a.printHistory()))


