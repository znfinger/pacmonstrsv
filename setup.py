from setuptools import setup, Extension, find_packages
from Cython.Build import cythonize
import numpy
import sys

if ("install" in sys.argv) and sys.version_info < (2, 7, 0):
    print "pacmonstrsv requires Python 2.7"
    sys.exit(-1)

include_path = [numpy.get_include()]

setup(
    name = 'pacmonstrsv',
    url="https://bitbucket.org/znfinger/pacmonstrsv/",
    version='0.1',
    author='Matthew Pendleton',
    author_email='matthew.pendleton@mssm.edu',
    license=open('LICENSES.txt').read(),
    packages=find_packages(),
    include_package_data=True,
    scripts = ["scripts/PACmonSTRSV_inversion_simple.py",
               "scripts/makeSV_dataset.py",
               "scripts/makeSV_dataset_oscar.py"
               ],
#    ext_modules = cythonize("bashirlab/cython/*.pyx", include_path),
    include_dirs=[numpy.get_include()],
    platforms="any"
)

